`default_nettype none

module left_justified#(
parameter DWIDTH = 16
) (
input wire clk,
input wire reset,

input  wire [DWIDTH-1:0] tx0_data,
input  wire              tx0_valid,
output logic             tx0_ready,

input  wire [DWIDTH-1:0] tx1_data,  // not connected
input  wire              tx1_valid, // not connected
output logic             tx1_ready, // not connected

output logic [DWIDTH-1:0] rx0_data,
output logic              rx0_valid,
input  wire               rx0_ready, // not connected

output logic [DWIDTH-1:0] rx1_data,  // not connected
output logic              rx1_valid, // not connected
input  wire               rx1_ready, // not connected

input  wire  mclk,
output logic bclk,
input  wire  adcdat,
output logic adclrck,
output logic dacdat,
output logic daclrck
);

assign bclk = mclk;

logic [7:0] bit_cnt = 0;
always_ff@(negedge mclk)
  bit_cnt <= bit_cnt + 1;

always_ff@(negedge mclk) adclrck <= bit_cnt[7];
always_ff@(negedge mclk) daclrck <= bit_cnt[7];

logic dac0fifo_empty;
logic dac0fifo_rdreq;
logic dac0fifo_full;
logic [DWIDTH-1:0] dac0buf;

left_justified_dcfifo#(.W(DWIDTH)) dac0fifo (
  .aclr    (reset),
  .data    (tx0_data),
  .rdclk   (mclk),
  .rdreq   (dac0fifo_rdreq),
  .wrclk   (clk),
  .wrreq   (tx0_valid),
  .q       (dac0buf),
  .rdempty (dac0fifo_empty),
  .wrfull  (dac0fifo_full)
);

assign tx0_ready = !dac0fifo_full;

always_ff@(negedge mclk) begin
  dac0fifo_rdreq <= &bit_cnt;
  dacdat <= dac0buf[DWIDTH-1-bit_cnt];
end

logic adc0fifo_empty;
logic adc0fifo_wrreq;
logic [DWIDTH-1:0] adc0buf;

always_ff@(posedge mclk) begin
  adc0buf <= {adc0buf[DWIDTH-2:0],adcdat};
  adc0fifo_wrreq <= bit_cnt == DWIDTH;
end

always_ff@(posedge clk)
  rx0_valid <= !adc0fifo_empty;

left_justified_dcfifo#(.W(DWIDTH)) adc0fifo (
  .aclr    (reset),
  .data    (adc0buf),
  .rdclk   (clk),
  .rdreq   (1'b1),
  .wrclk   (mclk),
  .wrreq   (adc0fifo_wrreq),
  .q       (rx0_data),
  .rdempty (adc0fifo_empty),
  .wrfull  ()
);

endmodule

`default_nettype wire
