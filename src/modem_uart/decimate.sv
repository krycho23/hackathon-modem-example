
module decimate#( 
  parameter DIV_LOG2 = 5
)(
input wire clk,
input wire reset,

input  wire [15:0] in_data,
input  wire in_valid,
output reg  in_ready,

output reg [15:0] out_data,
output reg  out_valid,
input  wire out_ready
);

assign in_ready = out_ready;

logic valid_int;
logic valid_int_d;
logic [DIV_LOG2-1:0] div_cnt;
always@(posedge clk) begin
  if(in_valid)
    div_cnt <= div_cnt + 1;
  valid_int <= div_cnt == 2**(DIV_LOG2-1);
  valid_int_d <= valid_int;
  out_valid <= valid_int & ~valid_int_d;
  if(valid_int)
   out_data <= in_data;
  if(reset)
    div_cnt <= '0;
end

endmodule
